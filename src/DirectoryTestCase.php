<?php declare(strict_types=1);

namespace N11t\PHPUnit;

use N11t\PHPUnit\Exception\DirectoryTestCaseException;
use PHPUnit\Framework\TestCase;

abstract class DirectoryTestCase extends TestCase
{

    /**
     * @var string
     */
    private $unitTestDirectory;

    protected function setUp()
    {
        parent::setUp();

        $this->unitTestDirectory = $this->createUniqueCleanDirectory();
    }

    protected function tearDown()
    {
        parent::tearDown();

        $directory = $this->unitTestDirectory;

        if ($this->isCleanUpEnabled()) {
            $this->removeFilesRecursive($directory);

            if (is_dir($directory) && !rmdir($directory) && is_dir($directory)) {
                throw new DirectoryTestCaseException(sprintf('Directory %s could not be deleted.', $directory));
            }
        }
    }

    /**
     * If this method returns true, the tear down method will remove all files.
     *
     * @return bool
     */
    protected function isCleanUpEnabled(): bool
    {
        return false;
    }

    /**
     * Create a clean unique directory in the system temp directory to handle files.
     *
     * @return string
     * @throws \ReflectionException
     */
    private function createUniqueCleanDirectory(): string
    {
        $clazz = (new \ReflectionClass($this))->getShortName();

        $tmpDir = sys_get_temp_dir();

        $pathname = rtrim($tmpDir, '/') . '/' . $clazz;

        if (!is_dir($pathname) && !mkdir($pathname) && !is_dir($pathname)) {
            throw new DirectoryTestCaseException(sprintf('Directory "%s" was not created', $pathname));
        }

        $this->removeFilesRecursive($pathname);

        return $pathname;
    }

    /**
     * Remove all files in the given pathname.
     *
     * @param string $pathname
     */
    private function removeFilesRecursive(string $pathname)
    {
        if (!is_dir($pathname)) {
            return;
        }

        $iterator = new \RecursiveDirectoryIterator($pathname, \FilesystemIterator::SKIP_DOTS);
        /** @var \SplFileInfo $file */
        foreach ($iterator as $file) {
            if ($file->isDir()) {
                $this->removeFilesRecursive($file->getPathname());
                rmdir($file->getPathname());
            } else {
                unlink($file->getPathname());
            }
        }
    }

    /**
     * Get the path in temporary unit test directory.
     *
     * @param string $path
     * @return string
     */
    protected function getPath(string $path): string
    {
        return rtrim($this->unitTestDirectory, '/') . '/' . ltrim($path, '/');
    }

    /**
     * Create a new directory in the temporary unit test directory.
     *
     * @param string $path
     * @return string The pathname to created directory.
     * @throws DirectoryTestCaseException If directory already exists or could not be created.
     */
    protected function createDirectory(string $path): string
    {
        $pathname = $this->getPath($path);

        if (is_dir($pathname)) {
            throw new DirectoryTestCaseException(sprintf('Directory %s already exists.', $pathname));
        }

        if (!mkdir($pathname) && !is_dir($pathname)) {
            throw new DirectoryTestCaseException(sprintf('Directory "%s" was not created', $pathname));
        }

        return $pathname;
    }

    /**
     * Copy the source asset to target. All path are relative to asset directory
     * and temporary test directory.
     *
     * @param string $source
     * @param string $target
     * @return string
     * @throws DirectoryTestCaseException
     */
    protected function copyAsset(string $source, string $target): string
    {
        $sourceFile = rtrim($this->getAssetsDirectory(), '/') . '/' . $source;
        $targetFile = $this->getPath($target);

        if (!is_file($sourceFile) || !is_readable($sourceFile)) {
            throw new DirectoryTestCaseException(sprintf('File %s not found or could not be read.', $sourceFile));
        }

        if (!copy($sourceFile, $targetFile)) {
            throw new DirectoryTestCaseException(sprintf('File "%s" => "%s" was not copied', $sourceFile, $targetFile));
        }

        return $targetFile;
    }

    /**
     * Read content from asset file.
     *
     * @param string $asset The relative path to asset, based on the asset directory. See {@see getAssetsDirectory}
     * @return string The content.
     * @throws DirectoryTestCaseException
     */
    protected function assetGetContent(string $asset): string
    {
        $filename = rtrim($this->getAssetsDirectory(), '/') . '/' . $asset;

        if (!is_file($filename) || !is_readable($filename)) {
            $message = sprintf('File %s not found or not readable.', $filename);
            throw new DirectoryTestCaseException($message);
        }

        return file_get_contents($filename);
    }

    /**
     * Get the absolute path to the Assets directory.
     *
     * @return string
     */
    abstract protected function getAssetsDirectory(): string;
}
