<?php
declare(strict_types=1);

namespace N11t\PHPUnit\Exception;

class DirectoryTestCaseException extends \RuntimeException
{
}
